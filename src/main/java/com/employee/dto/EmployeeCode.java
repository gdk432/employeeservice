package com.employee.dto;

import java.util.List;

public class EmployeeCode {

	private List<Long> employeeCode;

	public List<Long> getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(List<Long> employeeCode) {
		this.employeeCode = employeeCode;
	}

	
}
