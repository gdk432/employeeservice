package com.employee.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.employee.dto.EmployeeCodesRequestDto;
import com.employee.dto.FetchEmployeeResponceDto;
import com.employee.entity.Employee;
import com.employee.exception.EmployeeNotFoundException;
import com.employee.repository.EmployeeRepository;
import com.employee.service.FetchEmployeeService;

@Service
public class FetchEmployeeServiceImpl implements FetchEmployeeService {

	@Autowired
	EmployeeRepository employeeRepository;

	private static final Logger LOGGER = LoggerFactory.getLogger(FetchEmployeeServiceImpl.class);

	/**
	 * Fetching Employee
	 */
	@Override
	public List<FetchEmployeeResponceDto> fetchEmployee(int pageNumber, int pageSize, String employeeStatus) {
		Page<Employee> employee;
		List<FetchEmployeeResponceDto> fetchEmployeeResponceDtoList = new ArrayList<>();
		Pageable pageable = PageRequest.of(pageNumber, pageSize);
		employee = employeeRepository.findByemployeeStatus(pageable, employeeStatus);

		LOGGER.info("exception for employee");
		if (employee.isEmpty()) {
			LOGGER.warn("no employee found");
			throw new EmployeeNotFoundException("Not a valid employee");
		}

		employee.stream().forEach(emp -> {
			FetchEmployeeResponceDto fetchEmployeeResponceDto = new FetchEmployeeResponceDto();
			BeanUtils.copyProperties(emp, fetchEmployeeResponceDto);
			fetchEmployeeResponceDtoList.add(fetchEmployeeResponceDto);

		});
		return fetchEmployeeResponceDtoList;
	}

	@Override
	public List<FetchEmployeeResponceDto> fetchNameEmployee(int pageNumber, int pageSize, String employeeStatus,
			String employeeName) {
		Page<Employee> employee;
		List<FetchEmployeeResponceDto> fetchEmployeeResponceDtoList = new ArrayList<>();
		Pageable pageable = PageRequest.of(pageNumber, pageSize);
		employee = employeeRepository.findByemployeeStatusAndemployeeNameContains(pageable, employeeStatus,
				employeeName);

		employee.stream().forEach(emp -> {
			FetchEmployeeResponceDto fetchEmployeeResponceDto = new FetchEmployeeResponceDto();
			BeanUtils.copyProperties(emp, fetchEmployeeResponceDto);
			fetchEmployeeResponceDtoList.add(fetchEmployeeResponceDto);

		});
		return fetchEmployeeResponceDtoList;

	}

	// for feign fetch employee

	@Override
	public List<FetchEmployeeResponceDto> fetchEmployees(EmployeeCodesRequestDto employeeCodesRequestDto) {

		List<FetchEmployeeResponceDto> fetchEmployeeResponceDtoList = new ArrayList<>();
		List<Employee> employeeList = employeeRepository
				.findByemployeeStatus(employeeCodesRequestDto.getEmployeeStatus());

		for (Employee emp : employeeList) {
			Long comparecode2 = emp.getEmployeeCode();
			for (Long employeeCode : employeeCodesRequestDto.getEmployeeCode()) {
				Long employeeCode1 = employeeCode;
				if (employeeCode1.equals(comparecode2)) {
					FetchEmployeeResponceDto fetchEmployeeResponceDto = new FetchEmployeeResponceDto();
					BeanUtils.copyProperties(emp, fetchEmployeeResponceDto);
					fetchEmployeeResponceDtoList.add(fetchEmployeeResponceDto);
				}
			}

		}

		return fetchEmployeeResponceDtoList;
	}

// for feign
	@Override
	public FetchEmployeeResponceDto fetchEmployeeWithCode(Long employeeCode) {

		Optional<Employee> employee = employeeRepository.findById(employeeCode);

		LOGGER.info("exception for employee");
		if (!employee.isPresent()) {
			LOGGER.warn("no employee found");
			throw new EmployeeNotFoundException("Not a valid employee");
		}
		Employee emp = employee.get();

		FetchEmployeeResponceDto fetchEmployeeResponceDto = new FetchEmployeeResponceDto();
		BeanUtils.copyProperties(emp, fetchEmployeeResponceDto);

		return fetchEmployeeResponceDto;
	}

}