package com.employee.service.impl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.employee.dto.UpdateEmployeeRequestDto;
import com.employee.entity.Employee;
import com.employee.exception.EmployeeNotFoundException;
import com.employee.repository.EmployeeRepository;
import com.employee.service.UpdateEmployeeService;

@Service
public class UpdateEmployeeServiceImpl implements UpdateEmployeeService {

	private static final Logger LOGGER = LoggerFactory.getLogger(RemoveEmployeeServiceImpl.class);
	@Autowired
	EmployeeRepository employeeRepository;

	/**
	 * Update Employee
	 */
	@Override
	public String updateEmployee(UpdateEmployeeRequestDto updateEmployeeRequestDto) {
		Optional<Employee> empRecord = employeeRepository.findById(updateEmployeeRequestDto.getEmployeeCode());

		
		LOGGER.info("exception for employee");
		if (!empRecord.isPresent()) {
			LOGGER.info(" employee not found exception ");
			throw new EmployeeNotFoundException("Not a valid employee");
		}

		Employee employee = empRecord.get();
		employee.setEmployeeCode(updateEmployeeRequestDto.getEmployeeCode());
		employee.setEmployeeDesignation(updateEmployeeRequestDto.getEmployeeDesignation());
		employee.setEmployeeEmailId(updateEmployeeRequestDto.getEmployeeEmailId());
		employee.setEmployeeExperience(updateEmployeeRequestDto.getEmployeeExperience());
		employee.setEmployeeGender(updateEmployeeRequestDto.getEmployeeGender());
		employee.setEmployeeLocation(updateEmployeeRequestDto.getEmployeeLocation());
		employee.setEmployeeName(updateEmployeeRequestDto.getEmployeeName());
		employee.setEmployeePhoneNumber(updateEmployeeRequestDto.getEmployeePhoneNumber());
		employee.setEmployeeStatus(updateEmployeeRequestDto.getEmployeeStatus());
		employeeRepository.save(employee);
		return "succefully updated";
	}

}
