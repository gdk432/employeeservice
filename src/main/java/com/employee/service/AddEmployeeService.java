package com.employee.service;

import com.employee.dto.AddEmployeeRequestDto;

public interface AddEmployeeService {

	String addEmployee(AddEmployeeRequestDto addEmployeeRequestDtoList) throws Exception;

}
