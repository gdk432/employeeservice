package com.employee.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.employee.dto.AddEmployeeRequestDto;
import com.employee.entity.Employee;
import com.employee.repository.EmployeeRepository;
import com.employee.service.AddEmployeeService;

@Service
public class AddEmployeeServiceImpl implements AddEmployeeService {

	@Autowired
	EmployeeRepository employeeRepository;

	/**
	 * add employee
	 */

	@Override
	public String addEmployee(AddEmployeeRequestDto addEmployeeRequestDtoList) throws Exception {
		String status = addEmployeeRequestDtoList.getEmployeeStatus();
		List<Employee> employeeList = new ArrayList<Employee>();
		addEmployeeRequestDtoList.getEmployeeDetailsDto().stream().forEach(employeeDto -> {
			Employee employee = new Employee();
			BeanUtils.copyProperties(employeeDto, employee);
			employee.setEmployeeStatus(status);
			employeeList.add(employee);

		});

		employeeRepository.saveAll(employeeList);

		return "success";
	}

}
