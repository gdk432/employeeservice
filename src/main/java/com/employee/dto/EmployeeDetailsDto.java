package com.employee.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class EmployeeDetailsDto {

	private Long employeeCode;

	@NotEmpty(message = "please enter a valid employeeName")
	@Size(min = 3, max = 100,  message = "employeeName should be of 100 characterstics only")
	private String employeeName;

	@NotEmpty(message = "please enter a valid Gender")
	@Size(min = 0, max = 1, message = "employeeGender should be of 1 character only")
	private String employeeGender;

	@NotEmpty(message = "please enter a valid Designation")
	@Size(min = 3, max = 100,message = "employeeDesignation should be of 100 characterstics only")
	private String employeeDesignation;

	@NotEmpty(message = "Please provide Email id")
	@Size(min = 3, max = 100, message = "mailId should be of 100 characterstics only")
	@Email
	private String employeeEmailId;
	
	@Size(min = 1, max = 3, message = "please enter a valid employeeExperience ")
	private Long employeeExperience;

	@NotEmpty(message = "Please provide a valid Mobile Number")
	@Pattern(regexp = "(^$|[0-9]{​​​​​​​10}​​​​​​​)", message = "Provide valid Mobile Number")
	@Size(min = 3, max = 10, message = " Mobile Number is of 10 digits")
	private Long employeePhoneNumber;

	@Size(min = 3, max = 100)
	private String employeeLocation;

	public Long getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(Long employeeCode) {
		this.employeeCode = employeeCode;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getEmployeeGender() {
		return employeeGender;
	}

	public void setEmployeeGender(String employeeGender) {
		this.employeeGender = employeeGender;
	}

	public String getEmployeeDesignation() {
		return employeeDesignation;
	}

	public void setEmployeeDesignation(String employeeDesignation) {
		this.employeeDesignation = employeeDesignation;
	}

	public String getEmployeeEmailId() {
		return employeeEmailId;
	}

	public void setEmployeeEmailId(String employeeEmailId) {
		this.employeeEmailId = employeeEmailId;
	}

	public Long getEmployeeExperience() {
		return employeeExperience;
	}

	public void setEmployeeExperience(Long employeeExperience) {
		this.employeeExperience = employeeExperience;
	}

	public Long getEmployeePhoneNumber() {
		return employeePhoneNumber;
	}

	public void setEmployeePhoneNumber(Long employeePhoneNumber) {
		this.employeePhoneNumber = employeePhoneNumber;
	}

	public String getEmployeeLocation() {
		return employeeLocation;
	}

	public void setEmployeeLocation(String employeeLocation) {
		this.employeeLocation = employeeLocation;
	}

}
