package com.employee.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.employee.dto.AddEmployeeRequestDto;
import com.employee.dto.AddEmployeeRequestTestDto;
import com.employee.dto.EmployeeCodesRequestDto;
import com.employee.dto.FetchEmployeeResponceDto;
import com.employee.dto.UpdateEmployeeRequestDto;
import com.employee.service.AddEmployeeService;
import com.employee.service.FetchEmployeeService;
import com.employee.service.RemoveEmployeeService;
import com.employee.service.UpdateEmployeeService;

@Validated
@RestController

/**
 * 
 * Controller for employee details operations
 * 
 *
 */
public class EmployeeController {

	@Autowired
	AddEmployeeService addEmployeeService;

	@Autowired
	FetchEmployeeService fetchEmployeeService;

	@Autowired
	RemoveEmployeeService removeEmployeeService;

	@Autowired
	UpdateEmployeeService updateEmployeeService;

	/**
	 * 
	 * @param addEmployeeRequestDtoList
	 * @return
	 * @throws Exception
	 */
	@PostMapping("/add")
	public String addEmployee(@Valid @RequestBody AddEmployeeRequestDto addEmployeeRequestDtoList) throws Exception {

		return addEmployeeService.addEmployee(addEmployeeRequestDtoList);
	}

	/**
	 * 
	 * @param pageNumber
	 * @param pageSize
	 * @param employeeStatus
	 * @return
	 */

	@GetMapping("/fetch")
	public List<FetchEmployeeResponceDto> fetchEmployee(@RequestParam int pageNumber, @RequestParam int pageSize,
			@RequestParam String employeeStatus) {
		return fetchEmployeeService.fetchEmployee(pageNumber, pageSize, employeeStatus);

	}

	/**
	 * 
	 * @param employeeCode
	 * @return
	 */
	@DeleteMapping("/remove")
	public String removeEmployee(@RequestParam Long employeeCode) {
		return removeEmployeeService.removeEmployee(employeeCode);

	}

	/**
	 * 
	 * @param updateEmployeeRequestDto
	 * @return
	 */
	@PatchMapping("/update")
	public String updateEmployee(@RequestBody UpdateEmployeeRequestDto updateEmployeeRequestDto) {

		return updateEmployeeService.updateEmployee(updateEmployeeRequestDto);

	}

	@PostMapping("/fetch/empdetails")
	public List<FetchEmployeeResponceDto> fetchEmployeeDetails(
			@RequestBody EmployeeCodesRequestDto employeeCodesRequestDto) {
		return fetchEmployeeService.fetchEmployees(employeeCodesRequestDto);

	}
	//for feign client
	@GetMapping("/fetch/empcode")
	public  FetchEmployeeResponceDto fetchEmployeeWithCode  (@RequestParam Long employeeCode) {
		return fetchEmployeeService.fetchEmployeeWithCode(employeeCode);
		
	}

}
