package com.employee.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.employee.entity.Employee;

@Repository
public interface EmployeeRepository  extends JpaRepository<Employee, Long>{

	Page<Employee> findByemployeeStatus(Pageable pageable, String employeeStatus);
//	@Query(value ="select u.* from user u where u.firstname=:firstname and u.lastname=:lastname",nativeQuery = true)
	@Query  (value ="select u.* From Employee u where u.employeeStatus =:employeeStatus and u.employeeName like:employeeName",nativeQuery = true)
	Page<Employee> findByemployeeStatusAndemployeeNameContains(@Param("employeeName") Pageable pageable, String employeeStatus, String employeeName);
	List<Employee> findByemployeeStatus(String employeeStatus);

}
