package com.employee.service;

import java.util.List;

import com.employee.dto.EmployeeCode;
import com.employee.dto.EmployeeCodesRequestDto;
import com.employee.dto.FetchEmployeeResponceDto;

public interface FetchEmployeeService {

	List<FetchEmployeeResponceDto> fetchEmployee(int pageNumber, int pageSize, String employeeStatus);

	List<FetchEmployeeResponceDto> fetchNameEmployee(int pageNumber, int pageSize, String employeeStatus,
			String employeeName);

	

	List<FetchEmployeeResponceDto> fetchEmployees(EmployeeCodesRequestDto employeeCodesRequestDto);

	FetchEmployeeResponceDto fetchEmployeeWithCode(Long employeeCode);

}
