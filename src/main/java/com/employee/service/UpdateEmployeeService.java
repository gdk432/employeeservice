package com.employee.service;

import com.employee.dto.AddEmployeeRequestDto;
import com.employee.dto.UpdateEmployeeRequestDto;

public interface UpdateEmployeeService {

	String updateEmployee(UpdateEmployeeRequestDto updateEmployeeRequestDto);

}
