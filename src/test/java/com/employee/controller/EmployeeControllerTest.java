package com.employee.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.employee.dto.AddEmployeeRequestDto;
import com.employee.dto.EmployeeDetailsDto;
import com.employee.service.AddEmployeeService;

@ExtendWith(MockitoExtension.class)
public class EmployeeControllerTest {

	@Mock
	AddEmployeeService addEmployeeService;

	@InjectMocks
	EmployeeController employeeController;

	static List<EmployeeDetailsDto> employeeList;
	static AddEmployeeRequestDto addEmployeeRequestDto;
	static EmployeeDetailsDto employeeDetailsDto;
	static String Status = "Active";

	@BeforeAll
	public static void setup() {
		employeeList = new ArrayList<>();
		addEmployeeRequestDto = new AddEmployeeRequestDto();
		addEmployeeRequestDto.setEmployeeStatus("Status");
		addEmployeeRequestDto.setEmployeeDetailsDto(employeeList);
		employeeDetailsDto = new EmployeeDetailsDto();
		employeeDetailsDto.setEmployeeCode(518L);
		employeeDetailsDto.setEmployeeDesignation("senior software");
		employeeDetailsDto.setEmployeeEmailId("vin@gmail.com");
		employeeDetailsDto.setEmployeeExperience(3L);
		employeeDetailsDto.setEmployeeGender("M");
		employeeDetailsDto.setEmployeeLocation("hyd");
		employeeDetailsDto.setEmployeeName("Vin");
		employeeDetailsDto.setEmployeePhoneNumber(9626L);
		employeeList.add(employeeDetailsDto);

	}

	@Test
	@DisplayName("addEmployee")
	public void addEmployeeTest() throws Exception {
		when(addEmployeeService.addEmployee(addEmployeeRequestDto)).thenReturn("success");
		String result = employeeController.addEmployee(addEmployeeRequestDto);
		assertEquals("success", result);

	}

}
