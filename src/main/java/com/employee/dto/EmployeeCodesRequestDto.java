package com.employee.dto;

import java.util.List;

public class EmployeeCodesRequestDto {
	
	private String employeeStatus;
	private List<Long> employeeCode;

	public List<Long> getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(List<Long> employeeCode) {
		this.employeeCode = employeeCode;
	}

	public String getEmployeeStatus() {
		return employeeStatus;
	}

	public void setEmployeeStatus(String employeeStatus) {
		this.employeeStatus = employeeStatus;
	}
	
	

}
