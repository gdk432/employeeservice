package com.employee.entity;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class EmployeeTest {

	static Employee employee;

	@BeforeAll
	public static void setup() {
		employee = new Employee();
		employee.setEmployeeName("dev");
		employee.setEmployeeCode(51848L);
		employee.setEmployeeDesignation("software");
		employee.setEmployeeEmailId("dev@gmail.com");
		employee.setEmployeeExperience(3L);
		employee.setEmployeeGender("M");
		employee.setEmployeeLocation("hyd");
		employee.setEmployeePhoneNumber(9626L);
		employee.setEmployeeStatus("active");
	}

	@Test
	public void getEmployeeName() {
		String result = employee.getEmployeeName();
		assertEquals("dev", result);
	}

	@Test
	public void getEmployeeCode() {
		Long result = employee.getEmployeeCode();
		assertEquals(51848L, result);
	}

	@Test
	public void getEmployeeDesignation() {
		String result = employee.getEmployeeDesignation();
		assertEquals("software", result);
	}

	@Test
	public void getEmployeeEmailId() {
		String result = employee.getEmployeeEmailId();
		assertEquals("dev@gmail.com", result);
	}

	@Test
	public void getEmployeeExperience() {
		Long result = employee.getEmployeeExperience();
		assertEquals(3L, result);
	}

}
