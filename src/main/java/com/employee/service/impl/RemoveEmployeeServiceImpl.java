package com.employee.service.impl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.employee.entity.Employee;
import com.employee.exception.EmployeeNotFoundException;
import com.employee.repository.EmployeeRepository;
import com.employee.service.RemoveEmployeeService;

@Service
public class RemoveEmployeeServiceImpl implements RemoveEmployeeService {

	private static final Logger LOGGER = LoggerFactory.getLogger(RemoveEmployeeServiceImpl.class);

	@Autowired
	EmployeeRepository employeeRepository;

	/**
	 * Delete Employee
	 */
	@Override
	public String removeEmployee(Long employeeCode) {
		Optional<Employee> empRecord = employeeRepository.findById(employeeCode);

		LOGGER.info("exception for employee");
		if (!empRecord.isPresent()) {
			LOGGER.info("employee does not  exist  ");
			throw new EmployeeNotFoundException("Not a valid employee");
		}

		Employee employee = empRecord.get();
		employee.setEmployeeStatus("Inactive");
		employeeRepository.save(employee);

		return "succesully deleted";
	}

}
