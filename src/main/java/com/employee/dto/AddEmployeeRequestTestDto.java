package com.employee.dto;

import javax.validation.constraints.NotEmpty;

public class AddEmployeeRequestTestDto {
	
	
	
	private Long employeeCode;
	
	@NotEmpty(message = "please enter a valid employeeName")
	private String employeeName;
	public Long getEmployeeCode() {
		return employeeCode;
	}
	public void setEmployeeCode(Long employeeCode) {
		this.employeeCode = employeeCode;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	
}
