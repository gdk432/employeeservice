package com.employee.dto;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class AddEmployeeRequestDto {
	
	@NotEmpty(message = "employeeStatus should not be empty")
	@Size(min = 3, max = 15, message = "employeeStatus should be of 15 characterstics only")
	private String employeeStatus;
	
	@NotEmpty(message = "list should not be empty")
	private List< @Valid EmployeeDetailsDto> employeeDetailsDto;

	public String getEmployeeStatus() {
		return employeeStatus;
	}

	public void setEmployeeStatus(String employeeStatus) {
		this.employeeStatus = employeeStatus;
	}

	public List<EmployeeDetailsDto> getEmployeeDetailsDto() {
		return employeeDetailsDto;
	}

	public void setEmployeeDetailsDto(List<EmployeeDetailsDto> employeeDetailsDto) {
		this.employeeDetailsDto = employeeDetailsDto;
	}
	
	

}
