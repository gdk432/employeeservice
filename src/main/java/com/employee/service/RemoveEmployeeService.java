package com.employee.service;

public interface RemoveEmployeeService {

	String removeEmployee(Long employeeCode);

}
